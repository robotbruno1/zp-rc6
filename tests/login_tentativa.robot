**** Settings ***
Documentation              Login tentativa


Resource                   ../resources/base.robot

# executa uma ou mais Keywords antes da execulção de todos os stepes de ada caso de teste
# Test Setup

# executa um ou mais Keyword somente uma vez antes de todos os casos de testes
Suite Setup       Start Session

# executa uma ou mais Keyword apos a execução de todos os stepes de cada caso de teste
# Test Teardown

# execta uma ou mais keyword uma única vez apos finalizar todos os casos de testes
Suite Teardown    Finish Session


Test Template              Tentativa de login

*** Keywords ***
Tentativa de login
    [Arguments]                           ${input_email}        ${input_senha}    ${output_mensagem}
    Acesso a página Login
    Submeto minhas credencias             ${input_email}        ${input_senha}
    Devo ver um toaster com a mensagem    ${output_mensagem}

*** Test Case ***

Senha incorreta            admin@zepalheta.com.br      abc123      Ocorreu um erro ao fazer login, cheque as credenciais.
Senha em branco            joao@gmail.com              ${EMPTY}    O campo senha é obrigatório
Email em branco            ${EMPTY}                    123456      O campo email é obrigatório!
Email e senha em branco    ${EMPTY}                    ${EMPTY}    Os campos email e senha não foram preenchidos!
