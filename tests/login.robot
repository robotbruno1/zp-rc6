**** Settings ***
Documentation    Login

Resource         ../resources/base.robot

# executa uma ou mais Keywords antes da execulção de todos os stepes de ada caso de teste
# Test Setup

# executa um ou mais Keyword somente uma vez antes de todos os casos de testes
Suite Setup       Start Session

# executa uma ou mais Keyword apos a execução de todos os stepes de cada caso de teste
# Test Teardown

# execta uma ou mais keyword uma única vez apos finalizar todos os casos de testes
Suite Teardown    Finish Session

*** Test Case ***
Login do Administrador
    Acesso a página Login
    Submeto minhas credencias    ${admin_user}    ${admin_pass}
    Devo ver a área logada

