*** Settings ***
Documentation     Cadastro de clientes

Resource          ../resources/base.robot


Suite Setup       Login Session
Suite Teardown    Finish Session

*** Test Case ***
Novo cliente
    [tags]        smoke
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...                                                                                            Bon Jovi                           00000014141            Rua dos Bugs, 1000    21999999999
    Quando faço a inclusão desse cliente
    Então devo ver a notificação:                                                                  Cliente cadastrado com sucesso!
    E esse cliente deve ser exibido na lista
Campos Obrigatórios
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...                                                                                            ${EMPTY}                           ${EMPTY}               ${EMPTY}              ${EMPTY}
    Quando faço a inclusão desse cliente
    Então devo ver a mensagem informando que os campos do cadastro de clientes são obrigatórios

Nome é obrigatório
    [Template]                                                                                     Validação de campos
    ${EMPTY}                                                                                       22222222222                        Rua dos Testes, 171    21980701429           Nome é obrigatório

CPF é obrigatório
    [Template]                                                                                     Validação de campos
    German Cano                                                                                    ${EMPTY}                           Rua dos Testes, 171    21980701429           CPF é obrigatório

Endereço é obrigatório
    [Template]                                                                                     Validação de campos
    German Cano                                                                                    22222222222                        ${EMPTY}               21980701429           Endereço é obrigatório

Telefone é obrigatório
    [Template]                                                                                     Validação de campos
    German Cano                                                                                    22222222222                        Rua dos Testes, 171    ${EMPTY}              Telefone é obrigatório

Telefone incorreto
    [Template]                                                                                     Validação de campos
    German Cano                                                                                    22222222222                        Rua dos Testes, 171    2198070142            Telefone inválido


*** Keywords ***
Validação de campos
    [Arguments]                                                                                    ${nome}                            ${cpf}                 ${endereco}           ${telefone}               ${saida}
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...                                                                                            ${nome}                            ${cpf}                 ${endereco}           ${telefone}
    Quando faço a inclusão desse cliente
    Então devo ver o texto:                                                                        ${saida}



