*** Settings ***
Documentation     Exclusão de clientes

Resource          ../resources/base.robot


Suite Setup       Login Session
Suite Teardown    Finish Session

*** Test Case ***
Remover cliente
    Dado que eu tenho um cliente indesejado:
    ...                                         Bob Dylan                          44444444444    Rua dos testes, 153    (21) 2.2222-2222
    E acesso a lista de clientes
    Quando eu removo esse cliente
    Então devo ver a notificação:               Cliente removido com sucesso!
    E esse cliente não deve aparecer na lista



