*** Settings ***
Documentation            Representação do menu lateral de navegação na área logada

*** Variables ***
${NAV_CUSTMERS}        css:a[href$=customers]

*** Keywords ***
Go To Customers
    Wait Until Element Is Visible    ${NAV_CUSTMERS}       5
    Click Element                    ${NAV_CUSTMERS}
