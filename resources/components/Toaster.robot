*** Settings ***
Documentation         Representação do Toaster que mostra as mensagens no sistema

*** Variables ***
${TOASTER_SUCCESS}    css:div[type=success] strong
${TOASTER_ERRO}       css:div[type=error] p

# Mensagens 
# Cadastro de usuário com sucesso
${msg_sucesso}             Cliente cadastrado com sucesso!